import melangePlugin from 'vite-plugin-melange'

export default {
  plugins: [melangePlugin()]
}
