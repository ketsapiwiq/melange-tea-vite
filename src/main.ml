external is_hot : bool = "hot" [@@bs.val] [@@bs.scope "import", "meta"]

external hot_accept : (unit -> unit) -> unit = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_accept_dep : string -> (string -> unit) -> unit = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_accept_deps : string array -> (string array -> unit) -> unit
  = "accept"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

external hot_dispose : (unit -> unit) -> unit = "dispose"
  [@@bs.val] [@@bs.scope "import", "meta", "hot"]

let container = Web.Document.getElementById "main"

let app_module = "/_build/default/src/app.bs.js"

let other_reloadable_modules = [|"/_build/default/src/dep.bs.js"|]

let run () =
  if Config.is_dev then
    if is_hot then
      let shutdown_fun = ref (App.start_hot_debug_app container None) in
      let reloadable_modules =
        Belt.Array.concat [|app_module|] other_reloadable_modules
      in
      hot_accept_deps reloadable_modules (fun _mods ->
          let new_start_fun :
                 Web_node.t Js.nullable
              -> App.model option
              -> unit
              -> App.model option =
            [%raw {|_mods[0].start_hot_debug_app|}]
          in
          shutdown_fun := new_start_fun container (!shutdown_fun ()) ;
          () )
    else App.start_debug_app container |. ignore
  else App.start_app container |. ignore

let _ = Js.Global.setTimeout (fun _ -> run () |. ignore) 0
