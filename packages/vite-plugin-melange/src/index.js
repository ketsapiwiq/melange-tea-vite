import { join } from 'path';
import { cwd } from 'process';
import { exec } from 'child_process';

const src_dir = join(cwd(), '/src')
const build_dir = join(cwd(), '/_build/default/src')

function compile() {
  let child = exec('esy bsb -make-world');
  child.stderr.pipe(process.stdout);
  return new Promise((resolve) => {
    child.on('exit', function(exitCode) {
      if (parseInt(exitCode) !== 0) {
        resolve(false);
      } else {
        resolve(true);
      }
    })
  })
}

export default function melangePlugin() {
  return {
    name: 'melange-plugin',
    enforce: 'pre',

    async buildStart() {
      if (await compile()) {
        this.warn('Compilation successful');
      } else {
        this.warn('Compilation failed');
      }
    },

    async transform(code, id) {
      if (id.startsWith(build_dir) && id.endsWith('.bs.js')) {
        this.addWatchFile(id.replace(build_dir, src_dir).slice(0, -6) + '.ml');
      }
      return false;
    }
  }
}
