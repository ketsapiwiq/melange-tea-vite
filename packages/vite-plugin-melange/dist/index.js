'use strict';

var path = require('path');
var process$1 = require('process');
var child_process = require('child_process');

const src_dir = path.join(process$1.cwd(), '/src');
const build_dir = path.join(process$1.cwd(), '/_build/default/src');

function compile() {
  let child = child_process.exec('esy bsb -make-world');
  child.stderr.pipe(process.stdout);
  return new Promise((resolve) => {
    child.on('exit', function(exitCode) {
      if (parseInt(exitCode) !== 0) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  })
}

function melangePlugin() {
  return {
    name: 'melange-plugin',
    enforce: 'pre',

    async buildStart() {
      if (await compile()) {
        this.warn('Compilation successful');
      } else {
        this.warn('Compilation failed');
      }
    },

    async transform(code, id) {
      if (id.startsWith(build_dir) && id.endsWith('.bs.js')) {
        this.addWatchFile(id.replace(build_dir, src_dir).slice(0, -6) + '.ml');
      }
      return false;
    }
  }
}

module.exports = melangePlugin;
